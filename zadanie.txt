Комплексное задание для зачета по дисциплине

«Управление разработкой информационных систем»

1. Создать под своей учетной записью новый репозиторий на bitbucket.org. Имя репозитория состоит из сокращенного названия группы, символа нижнего подчеркивания, фамилии студента и первой буквы его имени латинскими буквами. Пример: BI_2_Ivanov_A.

2. Создать задачу о наполнении wiki в репозитории.

3. Создать страницу вики с описанием первого пункта данного задания и изображением главной страницы репозитория.

4. Клонировать репозиторий на локальный компьютер.

5. Сделать первый commit в которым разместить содержимое данного файла в простом текстовом формате (zadanie.txt). Выполнить скриншоты при выполнении данного пункта.

6. Внести изменения в текст задания (добавить свою группу и фамилию в конце документа)

7. Выполнить второй commit в ветке master.

8. Создать ветку от первого commit-а ветки master. Название новой ветки CR01.

9. Поместить в первый commit ветки CR01 скриншоты выполненные при выполнении задания 5.

10. Выполнить слияние ветки CR01 в ветку master.

11. Удалить ветку CR01.

12. Выполнить синхронизацию с удаленным репозиторием!

13. Изменить содержимое файла zadanie.txt добавить время выполнения данного пункта задания. Выполнить commit.

14. Выполнить синхронизацию с удаленным репозиторием!

15. Дополнить последний commit, добавив в файл zadanie.txt информацию о количестве баллов рейтинга, набранных за семестр.

16. Выполнить принудительную синхронизацию с удаленным репозиторием.

17. Отправить ссылку преподавателю.

1-7 пункты: 10 баллов

8-9 пункты: +2 балл

10-12 пункты: +2 балла

13-15 пункты: +4 балла

16 -17 пункты: +2 балл

35181-БИ-2 Точилин

Время 13:41

Баллы 42 